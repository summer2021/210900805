# Final Project Report
## Project Information

- Project Name:
Implement Robocopy

- Scheme Description:
Implementing an opensource version of ROBOCOPY for Wine that emulates the expected behaviour close enough to be a drop-in replacement for the proprietary version and to fix Wine bug #43653 .

- Time Planning:
```
| Week    | Date                 | Work / Desigation                         | Done |
|---------|----------------------|-------------------------------------------|------|
| Week 1  | 07-01 until 07-04    | Creation the required source files        | yes  |
| Week 2  | 07-05 until 07-11    | Argument Parser and required internal     | yes  |
|         |                      | (configuration) structures                |      |
| Week 3  | 07-12 until 07-18    | Directory Traversal logic (Source Folder) | yes  |
|         |                      | Save Information about relevant (at this  |      |
|         |                      | stage: all) files in a specified folder   |      |
|         |                      | in a (linked list like) data structure    |      |
| Week 4  | 07-19 until 07-25    | *Buffer Week*                             | ---  |
| Week 5  | 07-26 until 08-01    | Allow the opening, reading and writing of | yes  |
|         |                      | files / folders incl. creation and        |      |
|         |                      | deletion of folders                       |      |
| Week 6  | 08-02 until 08-08    | (Open), Read and Write relevant files,    | yes  |
|         |                      | based on the previous read data structure |      |
|         |                      | Implement logging output and correct      |      |
|         |                      | return values                             |      |
| Week 7  | 08-09 until 08-15    | *Buffer Week*                             | ---  |
| Week 8  | 08-16 until 08-22    | Implement Windows compatible wildcard     | yes  |
|         |                      | support and filtering for files and dirs  |      |
|         |                      | Implement time and size based filtering   |      |
| Week 9  | 08-23 until 08-29    | Implement /mov, /move, /purge and /l      | yes  |
|         |                      | Implement /mir, /e, /s, /lev              |      |
| Week 10 | 08-30 until 09-05    | Implement filtering based on files        | yes  |
|         |                      | already present at the destination (/xn,  |      |
|         |                      | /xc, /xl, /xo, /xx                        |      |
| Week 11 | 09-06 until 09-12    | *Buffer Week*                             | ---  |
| Week 12 | 09-13 until 09-19    | Code Review / Patch improvements          | ---  |
| Week 13 | 09-20 until 09-26    | Write conformance tests for all flags,    | yes  |
|         |                      | that check for the correct return value   |      |
|         |                      | and end results)                          |      |
| Week 14 | 09-27 until 09-30    | *Final polish, Buffer Week*               | ---  |
```

## Project Summary

- Project Output:

    - **Implement an opensource implementation of robocopy, which supports some most widely used flags**:
      
      Completed, please feel free to take a look at the patches uploaded to the OSPP Gitlab
      (and the pre-compiled binaries for 32bit and 64bit systems)

    - **Code merged into wine upstream**:
      
      Not completed, as the code is still in code review. Of course, especially as the code is already completely written, I'll continue to do anything neccessary to get the patches into the Wine upstream, including any change that may be neccessary to already written patches, even after the OSPP Summer of Code.

    - **Benefit at least one real-world app in https://bugs.winehq.org/show_bug.cgi?id=43653**:
      
      Completed, all applications in the bug report do work with this version of Robocopy without any issue, as all neccessary flags are implemented. 
      However, as the code is not yet it the upstream, the bug could not be closed yet.

- Scheme Progress: 

  ![Screenshot1](screenshots/screenshot_1.png) ![Screenshot2](screenshots/screenshot_2.png)
  ![Screenshot3](screenshots/screenshot_3.png) ![Screenshot4](screenshots/screenshot_4.png)

  All features / flags planned in the initial project schedule are implemented.
  The sole exception of this is the `/fat` flag, as Wine does not offer Windows compatability in regards to 8.3 file names. Additionally, this flag does nothing on NTFS filesystems, meaning there is with high probability no application that depends on whether `/fat` is implemented. For this reason, it was removed from the scope of the project.
  
  Code Review was also done following / started according to the Project Schedule, however, the time exclusively allocated for this in the schedule by me was way too optimistic / short. This is also the reason why the code is not yet merged into the wine upstream.

- Problems and Solutions: 
  
  The greatest challenge surely was my inexperience with any mailing-list / git patch based development workflow, like the one used by the Wine project. This caused me to severly underestimate the time neccessary to complete code review and get patches committed to the upstream by a magnitude.
  
  This was also one of the educational parts of this project, as I learned a valueable lesson in project time estimation and how to interact correctly with this kind of mailing-list collaboration, that will help me make better estimations in the future.

- Development Quality: 

  Most of the changes neccessary in code review were based on stylistic / code style choices that the Wine project set for itself or due to the git-patch workflow, like moving (parts of) functions to other commits to get patches that are as atomic as possible.

- Communication and Feedback with Mentor:

  Mrs. Figura is one of the best mentors that anyone could wish for. I can't thank her enough for the invaluable help in navigating the do's and dont's of the Wine project and for the time she took for me, to provide code review outside of the mailing list.
